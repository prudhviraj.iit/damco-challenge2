from urllib import response
from app import NumpyEncoder, app
import unittest
import io
from utils import load_data
import numpy as np
import json 


class FlaskTestCase(unittest.TestCase):

    def test_resized_image(self):
        tester = app.test_client(self)
        response = tester.get('/resized_image')
        self.assertEqual(response.status_code, 200)
    
    def test_resized_image_type(self):
        tester = app.test_client(self)
        response = tester.get('/resized_image')
        self.assertEqual(response.content_type, "image/png")
    
    def test_load_data(self):
        data = load_data("data/img.csv")
        self.assertEqual( data.shape, (5460,201))

    def test_numpy_encoder(self):
        array = np.zeros(2)
        x = json.dumps( { "data" : array} , cls=NumpyEncoder)
        self.assertEqual('{"data": [0.0, 0.0]}', x)

    def test_frames_bad_request(self):
        tester = app.test_client(self)
        response = tester.get('/frames')
        self.assertEqual(response.status_code, 400)
    
    def test_frames_min_gt_max(self):
        tester = app.test_client(self)
        response = tester.get('/frames?min_depth=9100.1&max_depth=9000.1')
        self.assertEqual(response.status_code, 400)

    def test_frames(self):
        tester = app.test_client(self)
        response = tester.get('/frames?min_depth=9000.1&max_depth=9001.1')
        self.assertEqual(response.status_code, 200)
    
    def test_frames_image_type(self):
        tester = app.test_client(self)
        response = tester.get('/frames?min_depth=9000.1&max_depth=9001.1&type=image')
        self.assertEqual(response.content_type, "image/png")
    
    def test_frames_array_type_row(self):
        tester = app.test_client(self)
        response = tester.get('/frames?min_depth=9000.1&max_depth=9000.1&type=array')
        x = json.loads(json.loads(response.text)["data"]["frames"] )
        self.assertTrue( len(x) == 1)

    def test_frames_wrong_cmap(self):
        tester = app.test_client(self)
        response = tester.get('/frames?min_depth=9000.1&max_depth=9000.1&type=array&color_map=xyz')
        self.assertEqual( response.status_code , 400)
    
    def test_frames_camp(self):
        tester = app.test_client(self)
        response = tester.get('/frames?min_depth=9000.1&max_depth=9000.1&type=array&color_map=gray')
        self.assertEqual( response.status_code , 200)
    


if __name__ == "__main__":
    unittest.main()