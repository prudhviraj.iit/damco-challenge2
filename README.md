# Damco Challenge 2

## Stack
<b>Programming Language : </b> Python <br>
<b> DB :</b>  SQLite <br>
<b> Libraries : </b>  
<li>Flask for HTTP API. </li>
<li>Numpy and Pandas for data manipulation. </li>
<li>PIL for image manipulation. </li>
<li>Matplotlib for list of colormaps. </li>


## Requirements & Installation
### Requirements
Python 3.x
### Installation

Clone the project. <br>
Change the directory into cloned project. <br>
Run pip install -r requirements.txt <br>
<b> To start : </b>flask run -p 6200


## API

### Get resized image from db -  /resized_image

Returns resized image that is stored in <b>SQLite db</b>

CLOUD URL : http://3.109.59.30:6200/resized_image <br>
LOCAL URL : http://localhost:6200/resized_image 



### Get Frames - /frames
Returns frames or image based on params. By default returns Frames as array

##### params:
<b>min_depth : </b>  min depth of the frames<br>
<b>max_depth : </b> max depth of the frames <br>
<b>Example : </b> 
CLOUD URL : http://3.109.59.30:6200/frames?min_depth=9000.1&max_depth=9100.9 <br>
LOCAL URL: http://localhost:6200/frames?min_depth=9000.1&max_depth=9100.9

<b>type : </b> return type. Allowed values <b>array</b> or <b>image</b>. <br>
<b>Example : </b> <br>
type=image <br>
CLOUD URL : http://3.109.59.30:6200/frames?min_depth=9000.1&max_depth=9100.9&type=image <br>
LOCAL URL : http://localhost:6200/frames?min_depth=9000.1&max_depth=9100.9&type=image  <br>
type=array <br>
CLOUD URL : http://3.109.59.30:6200/frames?min_depth=9000.1&max_depth=9100.9&type=array <br>
LOCAL URL : http://localhost:6200/frames?min_depth=9000.1&max_depth=9100.9&type=array

<b>color_map : </b> color map to apply<br>
<b>Example : </b>:  <br>
Image with color map: 
CLOUD URL : http://3.109.59.30:6200/frames?min_depth=9000.1&max_depth=9100.9&type=image&color_map=rainbow_r <br>
LOCAL URL : http://localhost:6200/frames?min_depth=9000.1&max_depth=9100.9&type=image&color_map=rainbow_r

Frames with color map: 
CLOUD URL : http://3.109.59.30:6200/frames?min_depth=9000.1&max_depth=9100.9&color_map=rainbow_r <br>
LOCAL URL : http://localhost:6200/frames?min_depth=9000.1&max_depth=9100.9&color_map=rainbow_r

<b>Allowed values for color maps :</b> <br>
'Accent', 'Accent_r', 'Blues', 'Blues_r', 'BrBG', 'BrBG_r', 'BuGn', 'BuGn_r', 'BuPu', 'BuPu_r', 'CMRmap', 'CMRmap_r', 'Dark2', 'Dark2_r', 'GnBu', 'GnBu_r', 'Greens', 'Greens_r', 'Greys', 'Greys_r', 'OrRd', 'OrRd_r', 'Oranges', 'Oranges_r', 'PRGn', 'PRGn_r', 'Paired', 'Paired_r', 'Pastel1', 'Pastel1_r', 'Pastel2', 'Pastel2_r', 'PiYG', 'PiYG_r', 'PuBu', 'PuBuGn', 'PuBuGn_r', 'PuBu_r', 'PuOr', 'PuOr_r', 'PuRd', 'PuRd_r', 'Purples', 'Purples_r', 'RdBu', 'RdBu_r', 'RdGy', 'RdGy_r', 'RdPu', 'RdPu_r', 'RdYlBu', 'RdYlBu_r', 'RdYlGn', 'RdYlGn_r', 'Reds', 'Reds_r', 'Set1', 'Set1_r', 'Set2', 'Set2_r', 'Set3', 'Set3_r', 'Spectral', 'Spectral_r', 'Wistia', 'Wistia_r', 'YlGn', 'YlGnBu', 'YlGnBu_r', 'YlGn_r', 'YlOrBr', 'YlOrBr_r', 'YlOrRd', 'YlOrRd_r', 'afmhot', 'afmhot_r', 'autumn', 'autumn_r', 'binary', 'binary_r', 'bone', 'bone_r', 'brg', 'brg_r', 'bwr', 'bwr_r', 'cividis', 'cividis_r', 'cool', 'cool_r', 'coolwarm', 'coolwarm_r', 'copper', 'copper_r', 'cubehelix', 'cubehelix_r', 'flag', 'flag_r', 'gist_earth', 'gist_earth_r', 'gist_gray', 'gist_gray_r', 'gist_heat', 'gist_heat_r', 'gist_ncar', 'gist_ncar_r', 'gist_rainbow', 'gist_rainbow_r', 'gist_stern', 'gist_stern_r', 'gist_yarg', 'gist_yarg_r', 'gnuplot', 'gnuplot2', 'gnuplot2_r', 'gnuplot_r', 'gray', 'gray_r', 'hot', 'hot_r', 'hsv', 'hsv_r', 'inferno', 'inferno_r', 'jet', 'jet_r', 'magma', 'magma_r', 'nipy_spectral', 'nipy_spectral_r', 'ocean', 'ocean_r', 'pink', 'pink_r', 'plasma', 'plasma_r', 'prism', 'prism_r', 'rainbow', 'rainbow_r', 'seismic', 'seismic_r', 'spring', 'spring_r', 'summer', 'summer_r', 'tab10', 'tab10_r', 'tab20', 'tab20_r', 'tab20b', 'tab20b_r', 'tab20c', 'tab20c_r', 'terrain', 'terrain_r', 'turbo', 'turbo_r', 'twilight', 'twilight_r', 'twilight_shifted', 'twilight_shifted_r', 'viridis', 'viridis_r', 'winter', 'winter_r'



# Unit Testing
Run  python tests.py








