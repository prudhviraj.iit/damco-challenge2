import sqlite3
import pandas as pd
import numpy as np
from PIL import Image
import io


DATA_PATH = "data/img.csv"


def load_data(path):
    """
        Load img data from disk
    """
    data = np.loadtxt(open(path, "rb"), delimiter=",", skiprows=1, encoding="ISO-8859-1")
    return data

def load_data_api():
    """
        Read data from disk for api usage. 
        Returns pandas dataframe 
    """
    df = pd.read_csv(DATA_PATH, encoding="ISO-8859-1")
    return df

def df_to_image(array, resize = True):
    """
        convert numpy array to image and store in io buffer.
    """
    stream = io.BytesIO()
    temp = Image.fromarray(array[:, 1:]).convert("L")
    if resize:
        temp = temp.resize((150, array.shape[0]))
    temp.save(stream, "png")
    return stream


def insert_BLOB(name, stream):
    """
        insert blob image to sqlite db
    """
    try:
        sqliteConnection = sqlite3.connect('db/images.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")
        sqlite_insert_blob_query = """ INSERT INTO frames
                                  (name, photo) VALUES ( ?, ?)"""

        img = stream.getvalue()
        data_tuple = (name, img)
        cursor.execute(sqlite_insert_blob_query, data_tuple)
        sqliteConnection.commit()
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)

    finally:
        if sqliteConnection:
            sqliteConnection.close()




def write_to_file(data, filename):
    with open(filename, 'wb') as file:
        file.write(data)

def read_blob_data():
    """
        read blob data from sqlite db
    """
    stream  = io.BytesIO()
    try:
        sqliteConnection = sqlite3.connect('db/images.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")
        sql_fetch_blob_query = """SELECT * from frames order by id desc limit 1"""
        cursor.execute(sql_fetch_blob_query)
        record = cursor.fetchall()
        for row in record:
            print("Id = ", row[0], "Name = ", row[1])
            name = row[1]
            photo = row[2]
            stream.write(photo)
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read blob data from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("sqlite connection is closed")
    
    return stream


def init():
    """
        Runs when the code is imported in the app.py file.

        This is done to load load csv data of image and store in db at the start of the application
    """
    df = load_data(DATA_PATH)
    stream = df_to_image(df)
    insert_BLOB("full resized image", stream)


init()