from flask import Flask, request, send_file
import numpy as np 
import matplotlib as plt 
from matplotlib import image as img
from utils import load_data_api, read_blob_data
import json
from PIL import Image
import io

"""
    Initialize the flask app and load data.
"""
app = Flask(__name__)
app.data = load_data_api()


ALLOWED_TYPES = ["ARRAY", "IMAGE"] #allowed return types for frames
ALLOWED_CMAPS = ['Accent', 'Accent_r', 'Blues', 'Blues_r', 'BrBG', 'BrBG_r', 'BuGn', 'BuGn_r', 'BuPu', 'BuPu_r', 'CMRmap', 'CMRmap_r', 'Dark2', 'Dark2_r', 'GnBu', 'GnBu_r', 'Greens', 'Greens_r', 'Greys', 'Greys_r', 'OrRd', 'OrRd_r', 'Oranges', 'Oranges_r', 'PRGn', 'PRGn_r', 'Paired', 'Paired_r', 'Pastel1', 'Pastel1_r', 'Pastel2', 'Pastel2_r', 'PiYG', 'PiYG_r', 'PuBu', 'PuBuGn', 'PuBuGn_r', 'PuBu_r', 'PuOr', 'PuOr_r', 'PuRd', 'PuRd_r', 'Purples', 'Purples_r', 'RdBu', 'RdBu_r', 'RdGy', 'RdGy_r', 'RdPu', 'RdPu_r', 'RdYlBu', 'RdYlBu_r', 'RdYlGn', 'RdYlGn_r', 'Reds', 'Reds_r', 'Set1', 'Set1_r', 'Set2', 'Set2_r', 'Set3', 'Set3_r', 'Spectral', 'Spectral_r', 'Wistia', 'Wistia_r', 'YlGn', 'YlGnBu', 'YlGnBu_r', 'YlGn_r', 'YlOrBr', 'YlOrBr_r', 'YlOrRd', 'YlOrRd_r', 'afmhot', 'afmhot_r', 'autumn', 'autumn_r', 'binary', 'binary_r', 'bone', 'bone_r', 'brg', 'brg_r', 'bwr', 'bwr_r', 'cividis', 'cividis_r', 'cool', 'cool_r', 'coolwarm', 'coolwarm_r', 'copper', 'copper_r', 'cubehelix', 'cubehelix_r', 'flag', 'flag_r', 'gist_earth', 'gist_earth_r', 'gist_gray', 'gist_gray_r', 'gist_heat', 'gist_heat_r', 'gist_ncar', 'gist_ncar_r', 'gist_rainbow', 'gist_rainbow_r', 'gist_stern', 'gist_stern_r', 'gist_yarg', 'gist_yarg_r', 'gnuplot', 'gnuplot2', 'gnuplot2_r', 'gnuplot_r', 'gray', 'gray_r', 'hot', 'hot_r', 'hsv', 'hsv_r', 'inferno', 'inferno_r', 'jet', 'jet_r', 'magma', 'magma_r', 'nipy_spectral', 'nipy_spectral_r', 'ocean', 'ocean_r', 'pink', 'pink_r', 'plasma', 'plasma_r', 'prism', 'prism_r', 'rainbow', 'rainbow_r', 'seismic', 'seismic_r', 'spring', 'spring_r', 'summer', 'summer_r', 'tab10', 'tab10_r', 'tab20', 'tab20_r', 'tab20b', 'tab20b_r', 'tab20c', 'tab20c_r', 'terrain', 'terrain_r', 'turbo', 'turbo_r', 'twilight', 'twilight_r', 'twilight_shifted', 'twilight_shifted_r', 'viridis', 'viridis_r', 'winter', 'winter_r']
#allowed color maps


class NumpyEncoder(json.JSONEncoder):
    """
        Encode numpy array to be sent in json response. 
    """
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



@app.route("/resized_image", methods=['GET'])
def get_image_from_db():
    """
        Returns stored image from db
    """
    min = request.args.get("min_depth", None)
    stream = read_blob_data()
    stream.seek(0)
    return send_file(stream, mimetype='image/png', download_name="frames.png")

    
@app.route("/frames", methods=['GET'])
def get_frames():
    """
        Returns frames as an array or image. 

        params:
        - required
            - min_depth : min_depth of the frame
            - max_depth : max_depth of the frame
        - optional
            - type : return type of frames. array or image
            - color_map : Color map to be applied. Can be any value from the allowed values.
    """
    min = request.args.get("min_depth", None) 
    max = request.args.get("max_depth", None)
    type = request.args.get("type", "array")
    cmap = request.args.get("color_map", None)

    if min is None or max is None or type.upper() not in ALLOWED_TYPES:
        return { "error" : "Bad Request"}, 400

    if float(min) > float(max):
        return { "error" : "Bad Request", "message" : "Min Depth cannot be greater than max depth"}, 400
    
    if cmap is not None and cmap not in ALLOWED_CMAPS:
        return { "error" : "Bad Request", "message": "color map not allowed values", "allowed_color_maps" : ALLOWED_CMAPS}, 400

    image = app.data[ app.data["depth"].between(float(min), float(max))].iloc[:, 1:].values.astype(np.uint8)
    if cmap is not None:
        color_map = img.cm.get_cmap(cmap)
        colored_image = color_map(image)
        image = (colored_image[:, :, :3] * 255).astype(np.uint8)
    

    if type.upper() ==  "ARRAY":
        return { "data" : { "frames" : json.dumps(image, cls=NumpyEncoder)}} , 200
    elif type.upper() == "IMAGE" :
        stream  = io.BytesIO()
        temp = Image.fromarray(image)
        temp.save(stream, "png")
        stream.seek(0)
        return send_file(stream, mimetype='image/png', download_name="frames.png")


